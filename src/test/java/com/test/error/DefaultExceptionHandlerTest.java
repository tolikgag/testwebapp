package com.test.error;

import com.test.controller.AuthorController;
import com.test.repository.AuthorRepository;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthorController.class)
@EnableSpringDataWebSupport
public class DefaultExceptionHandlerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AuthorRepository authorRepository;

    @Test
    public void defaultErrorHandler() throws Exception {
        when(authorRepository.findOne(anyLong())).thenReturn(null);
        String authorId = "1";
        mockMvc.perform(get("/author/" + authorId)).
                andExpect(status().isOk()).
                andExpect(view().name("error")).
                andExpect(model().attribute("msg", "Author isn't found by id=" + authorId)).
                andExpect(model().attribute("url", Matchers.endsWith("/author/1"))).
                andReturn();
    }

}