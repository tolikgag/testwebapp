package com.test.controller;

import com.test.entity.Author;
import com.test.repository.AuthorRepository;
import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthorController.class)
@EnableSpringDataWebSupport
public class AuthorControllerMvcTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AuthorRepository authorRepository;
    
    private static List<Author> authors;

    @BeforeClass
    public static void beforeClass() {
        authors = new ArrayList<>();

        Author author = new Author();
        author.setId(1L);
        author.setName("Author1");
        authors.add(author);

        author = new Author();
        author.setId(2L);
        author.setName("Author2");
        authors.add(author);
    }

    @Test
    public void getEmptyAuthorsTest() throws Exception {
        Page<Author> authorPage = new PageImpl<>(emptyList());
        when(authorRepository.findAll(any(Pageable.class))).thenReturn(authorPage);
        mockMvc.perform(get("/authors")).
                andExpect(status().isOk()).
                andExpect(view().name("authors")).
                andExpect(model().attribute("authors", emptyList())).
                andExpect(model().attribute("authorPage", authorPage)).
                andReturn();
    }

    @Test
    public void getAllAuthorsTest() throws Exception {
        Page<Author> authorPage = new PageImpl<>(authors);
        when(authorRepository.findAll(any(Pageable.class))).thenReturn(authorPage);
        mockMvc.perform(get("/authors")).
                andExpect(status().isOk()).
                andExpect(view().name("authors")).
                andExpect(model().attribute("authors", authors)).
                andExpect(model().attribute("authorPage", authorPage)).
                andReturn();
    }

    @Test
    public void showExistedAuthorTest() throws Exception {
        Author author = authors.get(0);
        when(authorRepository.findOne(author.getId())).thenReturn(author);
        when(authorRepository.findAll()).thenReturn(authors);
        mockMvc.perform(get("/author/" + author.getId())).
                andExpect(status().isOk()).
                andExpect(view().name("author_details")).
                andExpect(model().attribute("author", author)).
                andReturn();
    }

    @Test
    public void showNonExistedAuthorTest() throws Exception {
        when(authorRepository.findOne(anyLong())).thenReturn(null);
        String authorId = "1";
        mockMvc.perform(get("/author/" + authorId)).
                andExpect(status().isOk()).
                andExpect(view().name("error")).
                andExpect(model().attribute("msg", "Author isn't found by id=" + authorId)).
                andReturn();
    }

    @Test
    public void editExistedAuthorTest() throws Exception {
        Author author = authors.get(0);
        when(authorRepository.findOne(author.getId())).thenReturn(author);
        mockMvc.perform(get("/author/edit/" + author.getId())).
                andExpect(status().isOk()).
                andExpect(view().name("author")).
                andExpect(model().attribute("author", author)).
                andReturn();
    }

    @Test
    public void editNonExistedAuthorTest() throws Exception {
        when(authorRepository.findOne(anyLong())).thenReturn(null);
        String authorId = "1";
        mockMvc.perform(get("/author/edit/" + authorId)).
                andExpect(status().isOk()).
                andExpect(view().name("error")).
                andExpect(model().attribute("msg", "Author isn't found by id=" + authorId)).
                andReturn();
    }

    @Test
    public void createAuthorTest() throws Exception {
        mockMvc.perform(get("/author/new")).
                andExpect(status().isOk()).
                andExpect(view().name("author")).
                andExpect(model().attribute("author", Matchers.any(Author.class))).
                andReturn();
    }

    @Test
    public void saveAuthorTest() throws Exception {
        Author author = authors.get(0);
        when(authorRepository.save(any(Author.class))).thenReturn(author);
        mockMvc.perform(post("/author").requestAttr("author", author)).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/author/" + author.getId())).
                andReturn();
    }

    @Test
    public void deleteAuthorTest() throws Exception {
        doNothing().when(authorRepository).delete(anyLong());
        mockMvc.perform(get("/author/delete/1")).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/authors")).
                andReturn();
    }


}
