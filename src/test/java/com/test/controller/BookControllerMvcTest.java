package com.test.controller;

import com.test.entity.Author;
import com.test.entity.Book;
import com.test.repository.AuthorRepository;
import com.test.repository.BookRepository;
import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)
@EnableSpringDataWebSupport
public class BookControllerMvcTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private AuthorRepository authorRepository;

    private static List<Book> books;
    private static List<Author> authors;

    @BeforeClass
    public static void beforeClass() {
        authors = new ArrayList<>();

        Author author = new Author();
        author.setName("Author1");
        authors.add(author);

        books = new ArrayList<>();

        Book book = new Book();
        book.setId(1L);
        book.setTitle("book1");
        book.setAuthor(author);
        books.add(book);

        book = new Book();
        book.setId(2L);
        book.setTitle("book2");
        book.setAuthor(author);
        books.add(book);
    }

    @Test
    public void getEmptyBooksTest() throws Exception {
        Page<Book> bookPage = new PageImpl<>(emptyList());
        when(bookRepository.findAll(any(Pageable.class))).thenReturn(bookPage);
        mockMvc.perform(get("/books")).
                andExpect(status().isOk()).
                andExpect(view().name("books")).
                andExpect(model().attribute("books", emptyList())).
                andExpect(model().attribute("bookPage", bookPage)).
                andReturn();
    }

    @Test
    public void getAllBooksTest() throws Exception {
        Page<Book> bookPage = new PageImpl<>(books);
        when(bookRepository.findAll(any(Pageable.class))).thenReturn(bookPage);
        mockMvc.perform(get("/books")).
                andExpect(status().isOk()).
                andExpect(view().name("books")).
                andExpect(model().attribute("books", books)).
                andExpect(model().attribute("bookPage", bookPage)).
                andReturn();
    }

    @Test
    public void showExistedBookTest() throws Exception {
        Book book = books.get(0);
        when(bookRepository.findOne(book.getId())).thenReturn(book);
        when(authorRepository.findAll()).thenReturn(authors);
        mockMvc.perform(get("/book/" + book.getId())).
                andExpect(status().isOk()).
                andExpect(view().name("book_details")).
                andExpect(model().attribute("book", book)).
                andReturn();
    }

    @Test
    public void showNonExistedBookTest() throws Exception {
        when(bookRepository.findOne(anyLong())).thenReturn(null);
        String bookId = "1";
        mockMvc.perform(get("/book/" + bookId)).
                andExpect(status().isOk()).
                andExpect(view().name("error")).
                andExpect(model().attribute("msg", "Book isn't found by id=" + bookId)).
                andReturn();
    }

    @Test
    public void editExistedBookTest() throws Exception {
        Book book = books.get(0);
        when(bookRepository.findOne(book.getId())).thenReturn(book);
        when(authorRepository.findAll()).thenReturn(authors);
        mockMvc.perform(get("/book/edit/" + book.getId())).
                andExpect(status().isOk()).
                andExpect(view().name("book")).
                andExpect(model().attribute("book", book)).
                andExpect(model().attribute("authors", authors)).
                andReturn();
    }

    @Test
    public void editNonExistedBookTest() throws Exception {
        when(bookRepository.findOne(anyLong())).thenReturn(null);
        String bookId = "1";
        mockMvc.perform(get("/book/edit/" + bookId)).
                andExpect(status().isOk()).
                andExpect(view().name("error")).
                andExpect(model().attribute("msg", "Book isn't found by id=" + bookId)).
                andReturn();
    }

    @Test
    public void createBookTest() throws Exception {
        when(authorRepository.findAll()).thenReturn(authors);
        mockMvc.perform(get("/book/new")).
                andExpect(status().isOk()).
                andExpect(view().name("book")).
                andExpect(model().attribute("book", Matchers.any(Book.class))).
                andExpect(model().attribute("authors", authors)).
                andReturn();
    }

    @Test
    public void saveBookTest() throws Exception {
        Book book = books.get(0);
        when(bookRepository.save(any(Book.class))).thenReturn(book);
        mockMvc.perform(post("/book").requestAttr("book", book)).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/book/" + book.getId())).
                andReturn();
    }

    @Test
    public void deleteBookTest() throws Exception {
        doNothing().when(bookRepository).delete(anyLong());
        mockMvc.perform(get("/book/delete/1")).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/books")).
                andReturn();
    }


}
