insert into author(id, name)
values
    (1, 'Author 1'),
    (2, 'Author 2'),
    (3, 'Author 3'),
    (4, 'Author 4'),
    (5, 'Author 5'),
    (6, 'Author 6');

insert into book(id, title, author_id)
values
    (1, 'Book 1', 1),
    (2, 'Book 2', 1),
    (3, 'Book 3', 2),
    (4, 'Book 4', 3),
    (5, 'Book 5', 4),
    (6, 'Book 6', 5),
    (7, 'Book 7', 6),
    (8, 'Book 8', 2),
    (9, 'Book 9', 2),
    (10, 'Book 10', 2),
    (11, 'Book 11', 2),
    (12, 'Book 12', 2),
    (13, 'Book 13', 2),
    (14, 'Book 14', 2),
    (15, 'Book 15', 2),
    (16, 'Book 16', 2);
