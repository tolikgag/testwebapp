package com.test.controller;

import com.test.entity.Author;
import com.test.entity.Book;
import com.test.repository.AuthorRepository;
import com.test.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BookController {

    private static final Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping(value = "books")
    public String all(Model model, @PageableDefault(value = 5) Pageable pageable){
        logger.debug("Get Books");
        Page<Book> bookPage = bookRepository.findAll(pageable);
        model.addAttribute("books", bookPage.getContent());
        model.addAttribute("bookPage", bookPage);
        return "books";
    }

    @GetMapping("book/{id}")
    public String show(@PathVariable Long id, Model model){
        logger.debug("Get Book by id={}", id);
        Book book = bookRepository.findOne(id);
        if(book == null) {
            throw new IllegalArgumentException("Book isn't found by id=" + id);
        }
        model.addAttribute("book", book);
        return "book_details";
    }

    @GetMapping("book/edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        logger.debug("Get Book for updating by id={}", id);
        Book book = bookRepository.findOne(id);
        if(book == null) {
            throw new IllegalArgumentException("Book isn't found by id=" + id);
        }
        model.addAttribute("book", book);
        model.addAttribute("authors", authorRepository.findAll());
        return "book";
    }

    @GetMapping("book/new")
    public String create(Model model){
        logger.debug("Create new Book");
        Author author = new Author();
        Book book = new Book();
        book.setAuthor(author);
        model.addAttribute("book", book);
        model.addAttribute("authors", authorRepository.findAll());
        return "book";
    }

    @PostMapping("book")
    public String save(Book book){
        logger.debug("Save Book {}", book);
        Book savedBook = bookRepository.save(book);
        return "redirect:/book/" + savedBook.getId();
    }

    @GetMapping("book/delete/{id}")
    public String delete(@PathVariable Long id){
        logger.debug("Delete Book by id={}", id);
        bookRepository.delete(id);
        return "redirect:/books";
    }

}
