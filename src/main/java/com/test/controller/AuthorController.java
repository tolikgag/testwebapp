package com.test.controller;

import com.test.entity.Author;
import com.test.repository.AuthorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AuthorController {

    private static final Logger logger = LoggerFactory.getLogger(AuthorController.class);

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping(value = "authors")
    public String all(Model model, @PageableDefault(value = 5) Pageable pageable){
        logger.debug("Get Authors");
        Page<Author> authorPage = authorRepository.findAll(pageable);
        model.addAttribute("authors", authorPage.getContent());
        model.addAttribute("authorPage", authorPage);
        return "authors";
    }

    @GetMapping("author/{id}")
    public String show(@PathVariable Long id, Model model){
        logger.debug("Get Author by id={}", id);
        Author author = authorRepository.findOne(id);
        if(author == null) {
            throw new IllegalArgumentException("Author isn't found by id=" + id);
        }
        model.addAttribute("author", author);
        return "author_details";
    }

    @GetMapping("author/edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        logger.debug("Get Author for updating by id={}", id);
        Author author = authorRepository.findOne(id);
        if(author == null) {
            throw new IllegalArgumentException("Author isn't found by id=" + id);
        }
        model.addAttribute("author", author);
        return "author";
    }

    @GetMapping("author/new")
    public String create(Model model){
        logger.debug("Create new Author");
        model.addAttribute("author", new Author());
        return "author";
    }

    @PostMapping("author")
    public String save(Author author){
        logger.debug("Save Author {}", author);
        Author savedAuthor = authorRepository.save(author);
        return "redirect:/author/" + savedAuthor.getId();
    }

    @GetMapping("author/delete/{id}")
    public String delete(@PathVariable Long id){
        logger.debug("Delete Author by id={}", id);
        authorRepository.delete(id);
        return "redirect:/authors";
    }

}
